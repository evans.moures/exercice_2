# Utilisez l'image Drupal:9 comme base
FROM drupal:9

# Installez git
RUN apt-get update \
    && apt-get install -y git \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# Changez le répertoire de travail
WORKDIR /var/www/html/themes

# Clonez le thème Bootstrap depuis Git
RUN git clone --branch 8.x-4.x --single-branch --depth 1 https://git.drupalcode.org/project/bootstrap.git \
    && chown -R www-data:www-data bootstrap

# Rétablissez le répertoire de travail par défaut
WORKDIR /var/www/html

